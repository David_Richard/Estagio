﻿using SiggaFuncionarios.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace SiggaFuncionarios.DataBase
{
    public static class FuncionarioDataBase
    {
        public static ObservableCollection<FuncionarioSigga> lstFuncSigga = new ObservableCollection<FuncionarioSigga>();

        internal static void LoadDataBase()
        {
            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 1,
                Nome = "Filipi",
                Profissao = "Analista de Sistemas",
                Setor = "Setor 1",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/022.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 2,
                Nome = "Diego",
                Profissao = "Coordenador de Suporte",
                Setor = "Setor 2",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/081.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 3,
                Nome = "Cristiane",
                Profissao = "Gerente de talentos",
                Setor = "Setor 3",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/008.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 4,
                Nome = "Amanda",
                Profissao = "Analista de RH",
                Setor = "Setor 4",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/102.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 5,
                Nome = "Gabriela",
                Profissao = "Analista Financeiro",
                Setor = "Setor 5",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/014.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 6,
                Nome = "Moisés",
                Profissao = "Analista de Suporte",
                Setor = "Setor 6",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/020.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 7,
                Nome = "Rodrigo",
                Profissao = "Analista de Sistemas",
                Setor = "Setor 7",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/154.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 8,
                Nome = "Atayde",
                Profissao = "Analista de Sistemas",
                Setor = "Setor 8",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/019.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 9,
                Nome = "Freddy",
                Profissao = "Analista de vendas",
                Setor = "Setor 9",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/143.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 10,
                Nome = "Roger",
                Profissao = "Analista de Sistemas",
                Setor = "Setor 10",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/085.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 11,
                Nome = "Bruno",
                Profissao = "Consultor Funcional",
                Setor = "Setor 11",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/111.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 12,
                Nome = "Adam",
                Profissao = "Web Designer",
                Setor = "Setor 12",
                UrlFoto = "http://sigga.com.br/colaboradores/cor/005.jpg"
            });

            lstFuncSigga.Add(new FuncionarioSigga()
            {
                Id = 13,
                Nome = "Leandro",
                Profissao = "Diretor de Suporte",
                
                UrlFoto = "http://sigga.com.br/colaboradores/cor/072.jpg"
            });
        }

        public static FuncionarioSigga GetById(int id)
        {
            try
            {
                return FuncionarioDataBase.lstFuncSigga.Where(f => f.Id == id).First();
            }
            catch (ArgumentNullException)
            {
                return new FuncionarioSigga();
            }
        }

        public static ObservableCollection<FuncionarioSigga> GetAllFuncionario()
        {

            return FuncionarioDataBase.lstFuncSigga;
        }

        public static int Insert(FuncionarioSigga func)
        {            
            FuncionarioDataBase.lstFuncSigga.Add(func);
            return 1;
        }

        //Não existe botão para delete
        public static bool Delete(FuncionarioSigga func)
        {
            return FuncionarioDataBase.lstFuncSigga.Remove(func);
        }
    }
}
