﻿using SiggaFuncionarios.DataBase;
using SiggaFuncionarios.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SiggaFuncionarios.Page
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditFuncionarioView : ContentPage
    {
        private FuncionarioSigga FuncionarioDB;
        private FuncionarioSigga FuncionarioCorrente;
              
        public EditFuncionarioView(int id)
        {
            InitializeComponent();

            FuncionarioDB = FuncionarioDataBase.GetById(id);

            FuncionarioCorrente = new FuncionarioSigga()
            {
                Id = FuncionarioDB.Id,
                Nome = FuncionarioDB.Nome,
                Profissao = FuncionarioDB.Profissao,
                Setor = FuncionarioDB.Setor,
                UrlFoto = FuncionarioDB.UrlFoto
            };
            BindingContext = FuncionarioCorrente;
        }
        
        private async void Button_Clicked(object sender, EventArgs e)
        {

            var func = FuncionarioDataBase.GetById(FuncionarioDB.Id);
            //Deleta o antigo registro
            FuncionarioDataBase.Delete(func);

            //Dados do antigo registro + novo registro
            func.Id = FuncionarioDB.Id;
            func.Nome = FuncName.Text;            
            func.Profissao = FuncPro.Text;
            func.Setor = FuncSetor.Text;
            
            //Inseri os dados
            FuncionarioDataBase.Insert(func);


            await Navigation.PushModalAsync(new FuncionarioView()); // troca do comando "await Navigation.PopModalAsync();" ...para atualizar "listFuncionario.ItemsSource"

        }
    }
}