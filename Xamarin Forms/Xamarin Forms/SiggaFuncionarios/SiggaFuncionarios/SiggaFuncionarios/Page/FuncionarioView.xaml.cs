﻿using SiggaFuncionarios.DataBase;
using SiggaFuncionarios.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SiggaFuncionarios.Page
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FuncionarioView : ContentPage
    {
        private FuncionarioSigga FuncionarioDB;
        private FuncionarioSigga FuncionarioCorrente;

        public FuncionarioView()
        {
            InitializeComponent();
            listFuncionario.ItemsSource = FuncionarioDataBase.lstFuncSigga;           

        }

        private async void listFuncionario_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selectedFuncionario = e.Item as FuncionarioSigga;
            await Navigation.PushModalAsync(new EditFuncionarioView(selectedFuncionario.Id));
            

        }
    }
}