﻿using SiggaFuncionarios.Model;
using SiggaFuncionarios.Page;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace SiggaFuncionarios
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            FuncionarioSigga objFuncionario = new FuncionarioSigga();
            objFuncionario.LoadDataBase();

            MainPage = new FuncionarioView();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
