﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiggaFuncionarios.DataBase;

namespace SiggaFuncionarios.Model
{
    public class FuncionarioSigga : IModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Profissao { get; set; }
        public string UrlFoto { get; set; }

        public string Setor { get; set; } 


        public FuncionarioSigga()
        {
        }

        public void LoadDataBase()
        {
            FuncionarioDataBase.LoadDataBase();
        }

    }
}
